# estudo node express

## Rodando a aplicação
 - Copiar .env.example para .env
 - Para instalar as bibliotecas execute: `npm install`
 - Para rodar a aplicação execute: `npm run dev`

## Rodando mongodb local
 - execute o comando: `docker-compose up -d`
 - **Essa ação ocupará as portas 27017 e 5680**
## Postman Público
 - https://documenter.getpostman.com/view/14167008/TzCJepRo

## Release

 - [X] Criação de sistema de rotas
 - [x] Criação de primeiros modelos
 - [x] Criação de area de helpers
 - [x] Criação autenticação
 - [x] Criação middleware de validação de token JWT
 - [x] Configuração de casos de testes
 - [x] Criação grupos de usuarios (roles)
 - [ ] Criação autorização de gupos de usuarios (permissions)
 - [ ] Implementação de container com Swagger

## Boas praticas para nomenclaturas
 
 - Models usam nomes no **singular**
 - Nomenclatura em geral deve ser em inglês