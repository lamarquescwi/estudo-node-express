import type {Config} from '@jest/types';
import {defaults} from 'jest-config';

export default async (): Promise<Config.InitialOptions> => {
    return {
        bail: true,
        verbose: true,
        moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
        roots: [
            "<rootDir>/src",
            "<rootDir>/test",
        ],
        testMatch: [
            "<rootDir>/test/*.test.{ts,tsx}",
            "<rootDir>/test/**/*.test.{ts,tsx}",
        ],
        transform: {
            "^.+\\.(ts|tsx)$": "ts-jest"
        },
        testEnvironment: 'node'
    };
};

