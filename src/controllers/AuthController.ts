import {Request, Response} from "express";
import {UserRepository} from "../repositories/UserRepository";
import {Hash} from "../helpers/Hash";
import {RequestIp} from "../helpers/RequestIp";
import {TokenJwt} from "../helpers/TokenJwt";
import {SessionTokenRepository} from "../repositories/SessionTokenRepository";

class AuthController {

    async auth(req: Request, res: Response) {
        const token = req.headers.authorization;
        // @ts-ignore
        const tokenDecoded = await TokenJwt.verify(token);
        // @ts-ignore
        if (tokenDecoded?.error) {
            // @ts-ignore
            return res.status(401).send({auth: false, message: tokenDecoded?.error});
        }
        res.json(tokenDecoded);
    }

    async register(req: Request, res: Response) {
        const {name, lastName, email, password} = req.body;
        const user = {
            name: name,
            lastName: lastName,
            email: email,
            password: await Hash.create(password),
            isActive: true
        };
        res.json(await UserRepository.store(user));
    }

    async login(req: Request, res: Response) {
        const {email, password} = req.body;
        const user = await UserRepository.findByEmail(email);
        if (user != null && await Hash.check(password, user.password)) {
            const token = await TokenJwt.create({
                _id: user._id,
                name: user.name,
                email: user.email
            });
            const sessionToken = await SessionTokenRepository.store({
                userId: user._id,
                ip: await RequestIp.getIp(req),
                token: token,
                userAgent: req.headers['user-agent'],
                isActive: true
            });
            res.json({token: sessionToken.token});
        } else {
            res.status(401).json({error: "Unauthorized"});
        }
    }

}

export {AuthController}