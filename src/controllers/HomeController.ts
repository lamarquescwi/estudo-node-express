import {Request, Response} from 'express'

class HomeController {
    async index(req: Request, res: Response) {
        const navegador = req.headers;
        res.json({'status': true, navegador: navegador});
    }
}

export {HomeController}