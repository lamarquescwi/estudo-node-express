import {Request, Response} from "express";
import {UserRepository} from "../repositories/UserRepository";
import {Hash} from "../helpers/Hash";

class UsersController {

    async index(req: Request, res: Response)
    {
        res.json(await UserRepository.all())
    }

    async show(req: Request, res: Response)
    {
        const user = await UserRepository.find(req.params.id);
        if(user !== null) {
            res.json(user);
        }
        res.status(404).json({error: 'User Not Found'});
    }

    async store(req: Request, res: Response)
    {
        const {name, lastName, email, password, isActive} = req.body;
        const user = {
            name: name,
            lastName: lastName,
            email: email,
            password: await Hash.create(password),
            isActive: isActive
        };
        res.json(await UserRepository.store(user));
    }

    async update(req: Request, res: Response)
    {
        if (req.body.password) {
            req.body.password = await Hash.create(req.body.password);
        }
        res.json(await UserRepository.update(req.params.id, req.body));
    }

    async delete(req: Request, res: Response)
    {
        res.json(await UserRepository.delete(req.params.id));
    }
}

export {UsersController};