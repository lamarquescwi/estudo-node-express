import mongoose from "mongoose";



export default (db: string) => {
    const connect = () => {
        mongoose
            .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
            .then(() => {})
            .catch((error) => {
                console.log(error);
                return process.exit(1);
            });
    };
    connect();

    mongoose.connection.on("disconnected", connect);
};