import * as bcrypt from 'bcrypt';

class Hash {

    static async create(plainTextPassword: string) {
        const saltRounds = parseInt(process.env.BCRYPT_SALT || "default");
        return bcrypt.hash(plainTextPassword, saltRounds);
    }

    static async check(plainTextPassword: string, hash: string) {
        return await bcrypt.compare(plainTextPassword, hash);
    }

}

export {Hash}