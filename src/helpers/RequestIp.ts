import * as Ip from '@supercharge/request-ip';
import {Request} from "express";

class RequestIp {
    static async getIp(req: Request) {
        return Ip.getClientIp(req);
    }
}

export {RequestIp}