import jsonwebtoken from 'jsonwebtoken';

class TokenJwt {
    static async create(data: object) {
        const sign = {
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            data: data
        }
        const jwtSalt = process.env.JWTSALT || "default";
        return jsonwebtoken.sign(sign, jwtSalt);
    }

    static async verify(token: string) {
        const jwtSalt = process.env.JWTSALT || "default";
        return jsonwebtoken.verify(token.replace('Bearer ', ''), jwtSalt, (err, decoded) => {
            return {
                'data': (decoded === undefined) ? false : decoded,
                'error': err?.message
            };
        });
    }
}

export {TokenJwt}