import {NextFunction, Request, Response} from "express";
import {SessionTokenRepository} from "../repositories/SessionTokenRepository";
import {TokenJwt} from "../helpers/TokenJwt";

class Auth {
    static async check(req: Request, res: Response, next: NextFunction) {
        const token = req.headers.authorization != undefined ? req.headers.authorization : '';
        const tokenDecoded = await TokenJwt.verify(token);

        // @ts-ignore
        if (tokenDecoded?.error) {
            // @ts-ignore
            return res.status(401).send({error: tokenDecoded?.error});
        }

        const tokenActive = await SessionTokenRepository.findByTokenIsActiveTrue(token.replace('Bearer', '').toString().trim());

        if (!tokenActive) {
            return res.status(401).send({error: 'Unauthorized'});
        }

        next();
    }
}

export {Auth}