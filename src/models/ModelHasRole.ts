import mongoose, {Schema, Model, Document} from "mongoose";

interface IModelHasRole extends Document {
    _id?: string;
    ModelId: string;
    roleModel: string;
    role: string;
}

const ModelHasRoleSchema: Schema = new Schema({
    ModelId: {type: "string", required: true},
    roleModel: {type: "string", required: true},
    role: {type: "string", required: true},
})

const ModelHasRole: Model<IModelHasRole> = mongoose.model<IModelHasRole>('ModelHasRole', ModelHasRoleSchema);

export {ModelHasRole, IModelHasRole, ModelHasRoleSchema};