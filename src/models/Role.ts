import mongoose, {Schema, Model, Document} from "mongoose";

interface IRole extends Document {
    _id?: string;
    name: string;
}

const RoleSchema: Schema = new Schema({
    name: {type: "string", required: true},
})

const Role: Model<IRole> = mongoose.model<IRole>('Role', RoleSchema);

export {Role, IRole, RoleSchema};