import mongoose, {Schema, Model, Document} from "mongoose";

interface ISessionToken extends Document {
    _id?: string;
    userId: string;
    ip: string;
    token: string;
    userAgent: string;
    isActive: boolean;
    createdAt: Date;
}

const SessionTokenSchema: Schema = new Schema({
    userId: {type: "string", required: true},
    ip: {type: "string", required: true},
    token: {type: "string", required: true},
    userAgent: {type: "string", required: true},
    isActive: {type: "boolean", default: true},
    createdAt: {type: Date, default: Date.now, required: false}
})

const SessionToken: Model<ISessionToken> = mongoose.model<ISessionToken>('SessionToken', SessionTokenSchema);

export {ISessionToken, SessionTokenSchema, SessionToken}