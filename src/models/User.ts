import mongoose, {Schema, Model, Document} from "mongoose";

interface IUser extends Document {
    _id?: string;
    name: string;
    lastName: string;
    email: string;
    password: string;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;
}

const UserSchema: Schema = new Schema({
    name: {type: "string", required: true},
    lastName: {type: "string", required: true},
    email: {type: "string", required: true},
    password: {type: "string", required: true},
    isActive: {type: "boolean", default: true},
    createdAt: {type: Date, default: Date.now, required: false},
    updatedAt: {type: Date, default: Date.now, required: false}
})

const User: Model<IUser> = mongoose.model<IUser>('User', UserSchema);

export {User, IUser, UserSchema};