import {IModelHasRole, ModelHasRole} from "../models/ModelHasRole";

class ModelHasRolerepository {

    static async store(data: any) {
        return ModelHasRole.create(data).then((data: IModelHasRole) => {
            return data;
        }) .catch((error:Error) => {
            throw error;
        });
    }

    static async update(id: string, data: any)
    {
        return ModelHasRole.updateOne({_id: id}, data).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }

    static async delete(id: string) {
        return ModelHasRole.deleteOne({_id: id}).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }

    static async find(filter: any) {
        return ModelHasRole.find(filter).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }
}

export {ModelHasRolerepository}