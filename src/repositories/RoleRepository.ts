import {IRole, Role} from "../models/Role";

class RoleRepository {
    static async all()
    {
        return Role.find({});
    }

    static async findByName(name: string)
    {
        return Role.find({name: name});
    }

    static async find(id: string){
        return Role.findOne({_id: id});
    }

    static async store(data: any) {
        return Role.create(data).then((data: IRole) => {
            return data;
        }) .catch((error:Error) => {
            throw error;
        });
    }

    static async update(id: string, data: any) {
        return Role.updateOne({_id: id}, data).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }

    static async delete(id: string) {
        return Role.deleteOne({_id: id}).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }
}

export {RoleRepository}