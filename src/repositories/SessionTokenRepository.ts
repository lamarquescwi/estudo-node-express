import {ISessionToken, SessionToken} from "../models/SessionToken";

class SessionTokenRepository {
    static async findByTokenIsActiveTrue(token: string) {
        return SessionToken.findOne({token: token, isActive: true});
    }

    static async store(data: any) {
        return SessionToken.create(data);
    }
}

export {SessionTokenRepository}