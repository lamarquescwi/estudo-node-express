import {IUser, User} from "../models/User";
import {ModelHasRolerepository} from "./ModelHasRolerepository";

class UserRepository {
    static async all(): Promise<Array<IUser>>{
        return User.find({});
    }

    static async find(id: string){
        return User.findOne({_id: id});
    }

    static async findByEmail(email: string) {
        return User.findOne({email: email});
    }

    static async store(data: any) {
        return User.create(data).then((data: IUser) => {
            return data;
        }) .catch((error:Error) => {
            throw error;
        });
    }

    static async update(id: string, data: any) {
        // @ts-ignore
        return User.updateOne({_id: id}, data).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }

    static async delete(id: string) {
        return User.deleteOne({_id: id}).then(data => {
            return data;
        }).catch((error: Error) => {
            throw error;
        });
    }

    static async hasRole(roleName: string) {
        return ModelHasRolerepository.find({name: roleName});
    }
}

export {UserRepository}