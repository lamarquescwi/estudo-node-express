import { Router } from 'express';
import {HomeController} from "../controllers/HomeController";
import {UsersController} from "../controllers/UsersController";
import {AuthController} from "../controllers/AuthController";
import {Auth} from "../middlewares/Auth";

const router = Router();
const homeController = new HomeController();
const userController = new UsersController();
const authController = new AuthController();

router.get('/', homeController.index);

/** Rotas de autenticação */
router.post('/auth', authController.auth);
router.post('/auth/register', authController.register);
router.post('/auth/login', authController.login);

/** Rotas de genrenciamento de usuários */
router.get('/users', [Auth.check], userController.index);
router.get('/users/:id', [Auth.check], userController.show);
router.post('/users', [Auth.check], userController.store);
router.put('/users/:id', [Auth.check], userController.update);
router.delete('/users/:id', [Auth.check], userController.delete);

export {router};
