import Express from 'express';
import dotenv from 'dotenv';
import Connection from "./database/Connection";
import {router} from "./routes/routes";

dotenv.config({path:__dirname+'/../.env'});

const app = Express();
const port = process.env.APP_PORT || 3000;
const database_url = process.env.DATABASE_URL_TEST || "";
Connection(database_url);

app.use(Express.json());
app.use(Express.urlencoded({extended: true}));
app.use(router);

app.listen(port, () => console.log('Api rodando em http://localhost:' + port));