import dotenv from 'dotenv';
import Connection from "../src/database/Connection";
import {RoleRepository} from "../src/repositories/RoleRepository";
import {Role} from "../src/models/Role";

dotenv.config({path:__dirname+'/../.env'});
const database_url = process.env.DATABASE_URL_TEST || "";
Connection(database_url);

let id: string[] = [];

test('Cadastro de Roles', async () => {
    const RolesData = [
        "Super",
        "Administrator",
        "Editor",
        "User",
        "Anonimous",
    ];

    for (const roleName of RolesData) {
        const role = await RoleRepository.store({name: roleName});
        if(role._id) {
            expect(role).toBeInstanceOf(Role);
            id.push(role._id);
        }
    }


});

test("Update de roles", async () => {
    for (const roleId of id) {
        const roleUpdate = await RoleRepository.update(roleId, {name: "updated"});
        expect(roleUpdate.nModified).toBe(1);
    }
});

test("Delete roles", async () => {
    for (const roleId of id) {
        const roleDeleted = await RoleRepository.delete(roleId);
        expect(roleDeleted.deletedCount).toBe(1)
    }
});