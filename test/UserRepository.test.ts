import dotenv from 'dotenv';
import Connection from "../src/database/Connection";
import {Hash} from "../src/helpers/Hash";
import {UserRepository} from "../src/repositories/UserRepository";
import {User} from "../src/models/User";

dotenv.config({path:__dirname+'/../.env'});
const database_url = process.env.DATABASE_URL_TEST || "";
Connection(database_url);

let userId: string | null | undefined = null;

test('teste de conexao', () => {
    expect(true).toBe(true);
})

test('Cadastro de novo usuário.', async () => {
    const userData = {
        name: "Some",
        lastName: "Foo",
        email: "some.foo@test.tsx",
        password: await Hash.create('123Abc@?'),
        isActive: true
    }

    const user = await UserRepository.store(userData);
    userId = user._id;
    expect(user).toBeInstanceOf(User);
})

test('Update de Usuário', async () => {
    const userData = {
        name: "Some",
        lastName: "Foo2",
        email: "some.foo@test.tsx",
        password: await Hash.create('123Abc@?'),
        isActive: true
    }
    if(userId != null) {
        await UserRepository.update(userId, userData)
    }
    const user = await UserRepository.find(userId || "")
    expect(user?.lastName).toBe(userData.lastName);
})

test('Delete de Usuário', async () => {
    const del = await UserRepository.delete(userId || "")
    expect(del.deletedCount).toBe(1)
})

